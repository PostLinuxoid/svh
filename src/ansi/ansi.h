#if !defined(COLORS_H)
#define COLORS_H

#define ANSI_RESET      "\033[0m"
#define ANSI_BOLD       "\033[1m"
#define ANSI_UNDERLINE  "\033[4m"
#define ANSI_RED        "\033[31m"
#define ANSI_GREEN      "\033[32m"
#define ANSI_PURPLE     "\033[35m"

#endif /* COLORS_H */
