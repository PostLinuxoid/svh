#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <errno.h>

#include "ansi.h"
#include "eprintf.h"


extern const char *program_name;


void _ls(const char *path, int as_column) {
    DIR* dir = opendir(path);
    struct dirent *d = 0;
    const char separator = as_column ? '\n' : ' ';
    
    if (!dir || errno != 0) {
        epprintf(program_name, "failed to open di%c", 'r');
        perror("");
        exit(-1);
    }

    while ((d = readdir(dir))) {
        printf(ANSI_BOLD ANSI_PURPLE "%s %c" ANSI_RESET, d->d_name, separator);
    }

    closedir(dir);
}

int dir_exists(const char *path) {
    int res = 1;
    DIR* dir = opendir(path);
    if (ENOENT == errno) res = 0;
    closedir(dir);
    return res;
}
