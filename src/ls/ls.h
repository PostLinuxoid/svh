#if !defined(LS_H)
#define LS_H

extern void _ls(const char *path, int as_column);
extern int dir_exists(const char *path);

#endif /* LS_H */

