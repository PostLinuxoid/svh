#include <stdio.h>
#include <libgen.h>
#include <stdlib.h>
#include <string.h>

#include "argparse.h"
#include "ansi.h"
#include "help.h"
#include "eprintf.h"


const char *program_name = 0;


//---------------------------------------------------------------//
#define __EQ_ARGV(short_key, long_key, lk_len) (__strict_streq(argv[i], short_key, 2) || __strict_streq(argv[i], long_key, lk_len))

int __strict_streq(const char* str0, const char* str1, int len) {
    for (int i = 0; i < len; i++) {
        if (!str0[i] || !str1[i]) return 0;
        if (str0[i] != str1[i]) return 0;
    }
    return 1;
}
//---------------------------------------------------------------//




void argparse(const int argc, const char **argv, struct action_t *action) {
    char program_name_buffer[2048];
    strcpy(program_name_buffer, argv[0]);
    program_name = basename(program_name_buffer);

    if (argc < 2) {
        epprintf(program_name, "expected option or action (try -h or --help for more info%c", ')');
        exit(-1);
    }

    for (int i = 1; i < argc; i++) {
        if __EQ_ARGV("-h", "--help", 6) {
            action->action = PrintHelp;
        }
        else if __EQ_ARGV("-e", "--enabled", 9) {
            action->action = PrintEnabled;
        }
        else if (__strict_streq(argv[i], "-1", 2)) {
            action->action = PrintEnabled1;
        }
        else if __EQ_ARGV("-a", "--all", 5) {
            action->action = PrintAvailable;
        }
        else if ('e' == argv[i][0] || 'd' == argv[i][0]) {
            if (argc <= i + 1) {
                epprintf(program_name, "missing service name (try -h or --help for more info%c", ')');
                exit(-1);
            }
            if (__strict_streq(argv[i], "enable", 6)) {
                action->action = SvEnable;
                action->service_name = argv[++i];
            } 
            else if (__strict_streq(argv[i], "disable", 7)) {
                action->action = SvDisable;
                action->service_name = argv[++i];
            }
            else {
                epprintf(program_name, "enable or disable expected, found '%s' (try -h or --help for more info)", argv[i]);
                exit(-1);
            }
        }
        else {
            epprintf(program_name, "unexpected argument '%s' (try -h or --help for more info)", argv[i]);
            exit(-1);
        }
    }
}



