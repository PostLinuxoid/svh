#if !defined(ARGPARSE_H)
#define ARGPARSE_H


enum action 
{
    PrintHelp,      /* show help */
    PrintEnabled,   /* show enabled services */
    PrintEnabled1,  /* show enabled services as column */
    PrintAvailable, /* show all available services */
    SvEnable,       /* enable service */
    SvDisable,      /* disable service */
};

struct action_t {
    enum action action;         /* action: enable or disable service (use PrintHelp by defalt) */
    const char* service_name;   /* service name (if required, else NULL) */
};


/* program_name must be uninitialized (or NULL) pointer */
extern void argparse(const int argc, const char **argv, struct action_t *action);


#endif /* ARGPARSE_H */
