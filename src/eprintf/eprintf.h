#if !defined(EPRINTF_H)
#define EPRINTF_H


#include <stdio.h>
#include "ansi.h"

#define eprintf(fmt...) fprintf(stderr, ANSI_BOLD ANSI_RED fmt ANSI_RESET "\n")
#define epprintf(program_name, fmt, ...) fprintf(stderr, ANSI_BOLD "%s: " ANSI_RED fmt ANSI_RESET "\n", program_name, __VA_ARGS__)


#endif /* EPRINTF_H */


