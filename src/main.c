#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

#include "argparse.h"
#include "help.h"
#include "ls.h"
#include "eprintf.h"


#define SV_DIR "/etc/sv/"
#define ENABLED_SV_DIR "/var/service/" 



extern const char *program_name;



int main(int argc, const char **argv) {
    struct action_t action = {
        PrintHelp, // action
        0, // service name
    };

    argparse(argc, argv, &action);


    switch (action.action) {
        case PrintHelp:
            help();
            return 0;
        case PrintEnabled1:
            _ls(ENABLED_SV_DIR, 1);
            return 0;
        case PrintEnabled:
            _ls(ENABLED_SV_DIR, 0);
            return 0;
        case PrintAvailable:
            _ls(SV_DIR, 0);
            return 0;
        default: break;
    }



    char sv_path[8 + 256];
    char sv_link_path[13 + 256];

    sprintf(sv_path, SV_DIR "%s", action.service_name);
    sprintf(sv_link_path, ENABLED_SV_DIR "%s", action.service_name);


    if (!dir_exists(sv_path)) {
        epprintf(program_name, "service '%s' not found", action.service_name);
        return -1;
    }


    if (SvEnable == action.action) {
        printf("Creating symlink... ['%s' -> '%s']\n", sv_link_path, sv_path);
        symlink(sv_path, sv_link_path);
        if (errno != 0) {
            epprintf(program_name, "failed to enable '%s' service", action.service_name);
            return -1;
        }
        printf(ANSI_BOLD ANSI_GREEN "Service '%s' successfully enabled\n" ANSI_RESET, action.service_name);
        return 0;
    }
    else if (SvDisable == action.action) {
        printf("Removing symlink.. ['%s' -> '%s']\n", sv_link_path, sv_path);
        unlink(sv_link_path);
        if (errno != 0) {
            epprintf(program_name, "failed to disable '%s' service", action.service_name);
            return -1;
        }
        printf(ANSI_BOLD ANSI_GREEN "Service '%s' successfully disabled\n" ANSI_RESET, action.service_name);
        return 0;
    }


    return 0;
}


