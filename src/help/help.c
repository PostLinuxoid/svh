#include <stdio.h>

#include "ansi.h"


extern const char *program_name;


void help() {
    printf
    (
        /*----------/ string start /----------*/
        ANSI_RESET
        "%s -- Runit init system service helper\n"
        "\n"

        ANSI_BOLD ANSI_UNDERLINE
        "Usage:\n" ANSI_RESET
        "  " ANSI_BOLD "%s" ANSI_RESET " [OPTION] OR [ACTION [SVNAME]]\n"
        "\n"
        
        ANSI_BOLD ANSI_UNDERLINE
        "Actions:\n" ANSI_RESET
        "  " ANSI_BOLD "enable              "   ANSI_RESET "Enable service\n"
        "  " ANSI_BOLD "disable             "   ANSI_RESET "Disable service\n"
        "\n"

        ANSI_BOLD ANSI_UNDERLINE
        "Options:\n" ANSI_RESET
        "  " ANSI_BOLD "-a, --all           "  ANSI_RESET "Print all services\n"
        "  " ANSI_BOLD "-e, --enabled       "  ANSI_RESET "Print enabled services\n"
        "  " ANSI_BOLD "-1                  "  ANSI_RESET "Print enabled services as column\n"
        "  " ANSI_BOLD "-h, --help          "  ANSI_RESET "Print this help\n"
        "\n",
        /*-----------/ string end /-----------*/
        
        program_name, program_name
    );
}
