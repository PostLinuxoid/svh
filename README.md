# Service Helper
Runit init system service helper  
<span style="color:yellow">Note:</span> Program required root priveleges, thus I use doas utility (but You can use sudo)  


<br/>


Installation
------------
<span style="color:yellow">Note:</span> set the ***CC*** variable in Makefile to Your compiler (clang by default). 


### System-wide
    $ doas make clean install  


### Current user
    $ sed 's/\/usr\/local\/bin/~\/.local\/bin/' Makefile | tee Makefile
    $ make install


### Only build executable file
    $ make
    OR
    $ make clean build 
    

<br/>


Usage
-----
    $ doas svh [OPTION] OR [ACTION [SVNAME]]  


### Actions
**enable**  \<service name\>&emsp;&emsp;&nbsp;Enable service  
**disable** \<service name\>&emsp;&emsp;Disable service  


### Options
**-a, --all**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Print all services  
**-e, --enabled**&nbsp;&nbsp;Print enabled services  
**-1**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Print enabled services as column  
**-h, --help**&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Print help  


