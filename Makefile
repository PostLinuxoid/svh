TARGET  = svh
CC      = clang
CFLAGS 	= -Wall
SRC 	= src/main.c $(wildcard src/*/*.c)
OBJ 	= $(patsubst %.c, %.o, $(SRC))
INC 	= $(addprefix -I, $(wildcard src/*/))

INSTALL_DIR = /usr/local/bin


build: $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) -o $(TARGET)

%.o: %.c
	$(CC) $(CFLAGS) $(INC) -c $< -o $@


install: _install clean

_install: build
	mkdir -p $(INSTALL_DIR)
	mv -v $(TARGET) $(INSTALL_DIR)/$(TARGET)


clean:
	rm -rf $(TARGET) $(OBJ)

